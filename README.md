# Java激活工具


## Jetbrains 全家桶激活
> 在各大公域网站上看到ja-netfilter这个工具，用盗版IDEA的可能多多少少知道这个东西，这个工具暂时也没有什么其他用途，主要还是用于jetbrains家IDE的破解;
> 网上有人专门针对这款插件作了分析，这里不多加赘述，其实激活IDEA是每个人都能做到的事，完全不需要大费周章去破解它。
> 这里提供一个 Jetbrains 全家桶 激活方式，支持**2019及以上的所有版本**直接一键激活，永久使用。

* [IntelliJ IDEA 激活（建议收藏🔥）： 领先的Java和Kotlin IDE](https://arcstack.top/#/home)
* [PyCharm 激活（建议收藏🔥）：       面向专业开发者的Python IDE](https://arcstack.top/#/home)
* [WebStorm 激活（建议收藏🔥）：      最智能的JavaScript IDE](https://arcstack.top/#/home)

* [DataGrip 激活（建议收藏🔥）：      多种数据库，一个工具](https://arcstack.top/#/home)
* [DataSpell 激活（建议收藏🔥）：     专业数据科学家的IDE](https://arcstack.top/#/home)
* [GoLand 激活（建议收藏🔥）：        领先的Go IDE](https://arcstack.top/#/home)

* [PhpStorm 激活（建议收藏🔥）：      高效智能的PHP IDE](https://arcstack.top/#/home)
* [Rider 激活（建议收藏🔥）：         快速且强大的跨平台.NET IDE](https://arcstack.top/#/home)
* [RubyMine 激活（建议收藏🔥）：      最智能的Ruby与Rails IDE](https://arcstack.top/#/home)

* [AppCode 激活（建议收藏🔥）：       适用于iOS/macOS开发的智能 IDE](https://arcstack.top/#/home)
* [CLion 激活（建议收藏🔥）：         C和C++跨平台 IDE](https://arcstack.top/#/home)

## 其它工具激活
* [专业思维导图xmind8-pro 激活（建议收藏🔥）](https://arcstack.top/#/home)


## 优质计算机书籍导航

如果遇到链接失效或有其他问题，建议参考线上站点阅读：[arcstack.top](https://arcstack.top/pdf/)

也欢迎大家积极参与到项目中，一起推荐更多的资源，帮助更多的小伙伴！

笔者是一个人维护该仓库，资源需要一步步上传，我只能在上班之余慢慢更新，所以更新比较慢，还请谅解。如果在这里没用找到你想要的书，可以添加我的**个人微信**([javatodo](img/微信.png))，注明来意，我会慢慢更新上去的！

本仓库**持续更新**中，后续会陆续添加更多资源，**强烈建议大家 Star 本仓库**，找书的话可以直接 **Ctrl + F** 搜索，大幅提高效率。

此外，分享方式分为百度云盘分享,百度云盘如果失效可以通过公众号联系到我这边。

-   [C++](#C++)
-   [PHP](#PHP)
-   [Python语言](#Python语言)
-   [C#](#C#)
-   [java专栏]()
    -   [java初级](#java初级)
    -   [java高级](#java高级)
    -   [java面试题库梳理](#java面试题库梳理)
    -   [java虚拟机](#java虚拟机)
    -   [javaWeb](#javaWeb)
    -   [spring实战](#spring实战)
    -   [spring源码](#spring源码)
    -   [并发编程](#并发编程)
-   [计算机网络](#计算机网络)
-   [网络编程](#网络编程)
-   [WEB开发](#WEB开发)

-   [数据结构与算法](#数据结构与算法)
-   [高阶编程](#高阶编程)
-   [MySQL](#MySQL)
-   [Oracle](#Oracle)
-   [MongoDB](#MongoDB)
-   [SQL Server](#SQL Server)
-   [操作系统](#操作系统)
-   [工作流引擎](#工作流引擎)
-   [机器学习](#机器学习)
-   [大数据](#大数据)
-   [大数据相关](#大数据相关)
-   [分布式](#分布式)
-   [安全相关](#安全相关)
-   [架构设计](#架构设计)
-   [敏捷开发](#敏捷开发)
-   [项目管理](#项目管理)
-   [自然语言处理](#自然语言处理)




### C++
0.    《大规模C++程序设计》：[百度云链接](https://pan.baidu.com/s/11OIA-g0pVRedOAsSXlsr8w) 提取码：14y5
1.    《深入体验C语言项目开发》：[百度云链接](https://pan.baidu.com/s/1ujtqxCWlX2bYKZBRFsTHMw) 提取码：kvf5
2.    《实用C语言编程（第3版）》：[百度云链接](https://pan.baidu.com/s/1lUdpSh8hr1fsubXlfDSMQA) 提取码：hmfm
3.    《C++ Primer(第5版)(中文版)》：[百度云链接](https://pan.baidu.com/s/1ymszwALybfTk9tfAidDLyA) 提取码：makj
4.    《C和指针》：[百度云链接](https://pan.baidu.com/s/1W67iVx68B7USAaQUdc2CpA) 提取码：q8ue
5.    《C语言程序设计》：[百度云链接](https://pan.baidu.com/s/1E9cDdMZZRiGE0Rx_cVUB8Q) 提取码：8pu2
6.    《C语言的科学和艺术》：[百度云链接](https://pan.baidu.com/s/1EVGvR0xEefzNRHS-nX3q8A) 提取码：xt37
7.    《modern-cpp-tutorial》：[百度云链接](https://pan.baidu.com/s/1T7CtiBlM-LZFRE0zsDS7ag) 提取码：ispy

### PHP
0.    《PHP高级程序设计_模式、框架与测试》：[百度云链接](https://pan.baidu.com/s/1yCKQTsb8kBEICb7EhGg_qA) 提取码：yax7
1.    《PHP项目开发案例全程实录》：[百度云链接](https://pan.baidu.com/s/1nB9tozFwkDwDtn12Uctbog) 提取码：jegi


### Python语言
0.    《编写高质量代码 改善Python程序的91个建议》：[百度云链接](https://pan.baidu.com/s/17QIauW7K7_R6AyuniktrEw) 提取码：bqty
1.    《利用Python进行数据分析》：[百度云链接](https://pan.baidu.com/s/12HTXm1psv-_L5yrfyRQlAg) 提取码：cfuc
2.    《流畅的Python》：[百度云链接](https://pan.baidu.com/s/1cmchkn0IaRZpwVgL2Cx45w) 提取码：3934
3.    《深入浅出 Python》：[百度云链接](https://pan.baidu.com/s/12qZWf5IHNkWs7Gp0t0PI8g) 提取码：mujc
4.    《Django Web开发指南》：[百度云链接](https://pan.baidu.com/s/1jz3h7ktUkbY9X0E6IyNkcA) 提取码：nhhd
5.    《Python.Cookbook(第2版)中文版》：[百度云链接](https://pan.baidu.com/s/1IgrsUSriboDVTpTPE8vYvw) 提取码：tdd7
6.    《Python标准库中文版》：[百度云链接](https://pan.baidu.com/s/1HzIe7un_XWrAK-IG5_X7XQ) 提取码：i3aj
7.    《Python高级编程（法莱德）》：[百度云链接](https://pan.baidu.com/s/1Y9-Ak8GiAiDGynkE2dSXTw) 提取码：mhr5
8.    《Python核心编程(第2版)》：[百度云链接](https://pan.baidu.com/s/11UzLSHFUDcEECmzX6HT7_w) 提取码：qa78
9.    《Python灰帽子》：[百度云链接](https://pan.baidu.com/s/1nNrwngF6BwU1vJ6xLPVhlA) 提取码：rik7
10.    《python基础教程(第2版)》：[百度云链接](https://pan.baidu.com/s/1b_i6zP71sby-R-ooI0zzCg) 提取码：karr
11.    《Python源码剖析》：[百度云链接](https://pan.baidu.com/s/1fuF2HldhMsMI6wlg8bJYXQ) 提取码：n6jb
12.    《think in Python》：[百度云链接](https://pan.baidu.com/s/184UHTs379huJgQa9CJTN6w) 提取码：p2r4

### C#
0.    《编写高质量代码改善C#程序的157个建议》：[百度云链接](https://pan.baidu.com/s/1Zt3E-dn9peWZ-BarEqy8_w) 提取码：eidg
2.    《ASP.NET MVC4 WEB编程》：[百度云链接](https://pan.baidu.com/s/17wCr9zTPz1wcCFewMCWR4A) 提取码：ts5m
3.    《ASP.NET MVC4高级编程》：[百度云链接](https://pan.baidu.com/s/1g4Tj8v-rf6Ktea1hD3IFLQ) 提取码：32yw
4.    《ASP.NET MVC4框架揭秘》：[百度云链接](https://pan.baidu.com/s/1JSVWmx-RftnpRG5JnfwLKQ) 提取码：h18j
5.    《ASP.NET.4.0 揭秘(卷1)》：[百度云链接](https://pan.baidu.com/s/1fX0jFmbfX7dshDZmftJ3ow) 提取码：fqx9
6.    《ASP.NET.4.0 揭秘(卷2)》：[百度云链接](https://pan.baidu.com/s/1FfB9YxLOQodQNlU-ZSYcrw) 提取码：fu8m
7.    《ASP.NET本质论》：[百度云链接](https://pan.baidu.com/s/17MjUgiDv-vxdGECnrf6--Q) 提取码：hiv9
8.    《ASP.NET设计模式》：[百度云链接](https://pan.baidu.com/s/1o8FMLKLjY6h-AFDEPxYJ8w) 提取码：hgyy
9.    《C#本质论》：[百度云链接](https://pan.baidu.com/s/1Z95_PnOrzCoCGDQHgw_rUA) 提取码：kshs
10.    《C#程序开发范例宝典》：[百度云链接](https://pan.baidu.com/s/143zTg8C3EjLoiokGSjQPAg) 提取码：3yvi
11.    《C#入门经典(第3版)》：[百度云链接](https://pan.baidu.com/s/1MaQCEWJVWwnh1Cn6iPZXBw) 提取码：hfqg
12.    《C#入门经典(第5版)》：[百度云链接](https://pan.baidu.com/s/1j3dXz-sXsta_qv_GtZuHnA) 提取码：vgjh
13.    《C#与.NET程序员面试宝典》：[百度云链接](https://pan.baidu.com/s/1KtmJ8HwrjSjRfQWZWMw2yw) 提取码：4kb1
14.    《IT企业必读的200个.NET面试题》：[百度云链接](https://pan.baidu.com/s/1FJ7uMkNyiX1GZejJYB8p0w) 提取码：tj8g
15.    《NET本质论》：[百度云链接](https://pan.baidu.com/s/19QDr5cFYn3oKAjUkNnAuBA) 提取码：kfx3
16.    《NET应用程序架构设计 原则 模式与实践》：[百度云链接](https://pan.baidu.com/s/1_oxv324F8qEhqltjv_yTdg) 提取码：7ej1
17.    《WCF服务编程》：[百度云链接](https://pan.baidu.com/s/1_JO0sfC9Xau6rkiufJ5O6A) 提取码：nayn
18.    《WCF全面解析（上册）》：[百度云链接](https://pan.baidu.com/s/1niOGogBb7enk2PPBbdUf6A) 提取码：mj4g
19.    《WCF全面解析（下册）》：[百度云链接](https://pan.baidu.com/s/1GbwGhHuQA6rRs_UNozfOVw) 提取码：wrn3


### java初级
0.    《[Java语言程序设计-进阶篇(原书第8版)]》：[百度云链接](https://pan.baidu.com/s/1Qh9oNnySUKE9Az5LjWx6ZQ) 提取码：59jg
1.    《疯狂Java讲义(第3版)》.(李刚).[PDF]@ckook：[百度云链接](https://pan.baidu.com/s/1H-pINMdSlPF7PcEDlBykJw) 提取码：w6bm
2.    《Java 8函数式编程》_王群锋译_2015-04-01：[百度云链接](https://pan.baidu.com/s/1wyOJ5gt4PL3XOUg-Ee0w6w) 提取码：pn9v
3.    《Java JDK 9学习笔记》_林信良_2018-06-01：[百度云链接](https://pan.baidu.com/s/1V2mqn6JfOZwnN7chfK6abA) 提取码：b2c3
4.    《Java8实战》：[百度云链接](https://pan.baidu.com/s/14liKA34XW_s2Vy5EeikU8Q) 提取码：yhvs
5.    《Java9模块化开发核心原则与实践》_王净等译：[百度云链接](https://pan.baidu.com/s/1sG3_heopdh15xjLf5JzwPQ) 提取码：f7ua
6.    《Java函数式编程》：[百度云链接](https://pan.baidu.com/s/1gtqN_BTZQENT9xZWp-iPZQ) 提取码：hv9j
7.    《Java核心技术卷I：基础知识（原书第10版）》：[百度云链接](https://pan.baidu.com/s/1BRAHOFqAGrJOQikZf9KDew) 提取码：ig5h
8.    《Java核心技术卷II_高级特性(原书第9版)》：[百度云链接](https://pan.baidu.com/s/1BkYKYca0OIB8FL0sUClbyg) 提取码：b3p9
9.    《Java核心技术卷II_高级特性（原书第10版）》：[百度云链接](https://pan.baidu.com/s/10vWSV3UhH8iUPwOUHbcayw) 提取码：9uuy
10.    《Java8函数式编程》：[百度云链接](https://pan.baidu.com/s/1e6krDLFC8pzOJdLPeJOqSw) 提取码：met7


### java高级
0.    《大型网站性能优化实战从前端网络CDN到后端大促的全链路性能优化》_周涛明_2019-01-01：[百度云链接](https://pan.baidu.com/s/1ruQmeQ0MuYNShvwvCm4RJQ) 提取码：8u89
1.    《Effective_Java中文版（原书第3版）》_俞黎敏译_2018-12-11：[百度云链接](https://pan.baidu.com/s/1e-AlFdqHWC_SJuaePjoIhg) 提取码：kwct
2.    《阿里巴巴Java开发手册终极版v1.3.0》：[百度云链接](https://pan.baidu.com/s/1M8g3NA9iKSorDJxASgll3A) 提取码：x77v
3.    《分布式JAVA应用 基础与实践》：[百度云链接](https://pan.baidu.com/s/1XHsJTQMo3oj9f19CjfgF5w) 提取码：33u2
4.    《设计模式之禅 秦晓波》：[百度云链接](https://pan.baidu.com/s/13jQYtX84QZk0YPSLf2Imyg) 提取码：qt4k
5.    《深入理解Java虚拟机：JVM高级特性与最佳实践(第3版)》：[百度云链接](https://pan.baidu.com/s/1PYaNtwdHA4HR2xzqUg-lTQ) 提取码：sjt7
6.    《深入理解java虚拟机(原书第2版)》：[百度云链接](https://pan.baidu.com/s/1_RQ9kpBTQgU-37Hs3PK3iw) 提取码：jiff
7.    《实战Java虚拟机》：[百度云链接](https://pan.baidu.com/s/1Pu5ysBoJcTpfk1GaZdSiig) 提取码：r3v1
8.    《Effective Java中文版 (第2版) (Joshua Bloch)》：[百度云链接](https://pan.baidu.com/s/1o54hMt2hA2Eg28J1pWHI0Q) 提取码：6epz
9.    《Effective_Java_中文第二版》：[百度云链接](https://pan.baidu.com/s/1SK4PUtujTc5BZF2uLobw-A) 提取码：3w63
10.    《Elasticsearch服务器开发(第2版)》：[百度云链接](https://pan.baidu.com/s/1wURP3KzDiuE6jjz1lTMjLA) 提取码：uqdg
11.    《Head First Java 中文高清版》：[百度云链接](https://pan.baidu.com/s/1qkfoY4v6Dn-CwRmqPzvRQw) 提取码：fjyw
12.    《Head_First_Java_中文高清版》：[百度云链接](https://pan.baidu.com/s/1EX827-YSmxVheVY8vJWRFg) 提取码：6q6e
13.    《Java 8实战》：[百度云链接](https://pan.baidu.com/s/1e3hIK4dZ_A0CWCwL_TP_uw) 提取码：wpyt
14.    《Java_NIO_中文版》：[百度云链接](https://pan.baidu.com/s/11gUhLNYLI47U9cVIBQnnWg) 提取码：vm3q
15.    《Java编程思想第四版完整中文高清版》：[百度云链接](https://pan.baidu.com/s/1QGj3jSTj9SP6V4_KV2R-nA) 提取码：8frv
16.    《Java并发编程实践（中文版）》：[百度云链接](https://pan.baidu.com/s/1fJguRJDz__BnxcUgQQfHlg) 提取码：q2jn
17.    《Java并发编程实战》：[百度云链接](https://pan.baidu.com/s/1EAfybkEWx7AF0W0ZBYPJPQ) 提取码：nwhg
18.    《Java程序性能优化  让你的Java程序更快、更稳定》：[百度云链接](https://pan.baidu.com/s/1MgXv9mr6HEOlq6V-gUsf6w) 提取码：d6cv
19.    《Java核心技术》：[百度云链接](https://pan.baidu.com/s/1nj3mT7XjiD-pMeJsZOPCnw) 提取码：ctev
20.    《Java入门经典》：[百度云链接](https://pan.baidu.com/s/1BumKLliFOFQrPRNSo2XKKw) 提取码：mv46
21.    《Java性能权威指南》：[百度云链接](https://pan.baidu.com/s/1bZR8bFlMnZthSXFHm9IlDQ) 提取码：inwb
22.    《JAVA性能优化权威指南》：[百度云链接](https://pan.baidu.com/s/1AvtrRO-YSK0WzzqTo3l9vg) 提取码：pdjs
23.    《JavaTCPIPSocket编程(原书第2版)》：[百度云链接](https://pan.baidu.com/s/1F0MeMO792-1TxoVIlga8gg) 提取码：gk87
24.    《Maven实战》：[百度云链接](https://pan.baidu.com/s/14Lygw5s9ao71YK6EVIwGIQ) 提取码：j7g9
25.    《Thymeleaf(3.0)》：[百度云链接](https://pan.baidu.com/s/1RYYOCbBguPtslk2lRT4apQ) 提取码：qp69
26.    《TOMCAT权威指南》：[百度云链接](https://pan.baidu.com/s/1cnBtjXvpxK-swaXqNDYAeQ) 提取码：ren5


### java面试题库梳理
0.    《技术面试必备基础知识(CyC2018)》：[百度云链接](https://pan.baidu.com/s/12awb3OhB0d6ZhAoMop3U0g) 提取码：bchi
1.    《JAVA核心知识点整理》：[百度云链接](https://pan.baidu.com/s/1P9h9AqOregxQ5TwzwNXbiQ) 提取码：d7s1
2.    《JAVA面试核心知识点整理》：[百度云链接](https://pan.baidu.com/s/1Rfi517eLPh4nXSnSL5ipDg) 提取码：jc5d
3.    《JavaGuide面试突击版(V3.0)》：[百度云链接](https://pan.baidu.com/s/1H43WEW6Or5kVwmkLiFd5Kw) 提取码：3cf8


### java虚拟机
0.    《[深入理解Java虚拟机：JVM高级特性与最佳实践]》：[百度云链接](https://pan.baidu.com/s/1fsRcP_8BlinpAVXmFClcvw) 提取码：ftwi
1.    《揭秘Java虚拟机 JVM设计原理与实现》：[百度云链接](https://pan.baidu.com/s/1ToNLg4tyh5Nk0vtm0azLlw) 提取码：q4bw
2.    《深入理解Java虚拟机_JVM高级特性与最佳实践（第2版）》_周志明：[百度云链接](https://pan.baidu.com/s/1Wr-JjkNKz9hinjy58IjutA) 提取码：c6cj
3.    《深入理解JVM＆G1_GC 》：[百度云链接](https://pan.baidu.com/s/1KQqraPjQ_3X89_44w20SrQ) 提取码：gq1y
4.    《实战Java虚拟机：JVM故障诊断与性能优化》_葛一鸣：[百度云链接](https://pan.baidu.com/s/1lrVhIR-9WlnGEJVxudF3fQ) 提取码：hmfh
5.    《深入理解JAVA内存模型》：[百度云链接](https://pan.baidu.com/s/19c2Y5cEhgLfa8jPqePOBig) 提取码：ed2f
6.    《深入理解Java虚拟机_JVM高级特性与最佳实践（最新第二版）》：[百度云链接](https://pan.baidu.com/s/1322i-iOxYr1skUySH4reHg) 提取码：qakd


### javaWeb
0.    《[使用Java.Web服务构建SOA].(汉森).成保栋》：[百度云链接](https://pan.baidu.com/s/1Mf2XfPOKp3OtA3YzpBzoEA) 提取码：iyhp
1.    《Web服务器群集》：[百度云链接](https://pan.baidu.com/s/1rXz5MOXxAOeaeWzpHrWqrQ) 提取码：6vt6
2.    《高性能响应式Web开发实战》：[百度云链接](https://pan.baidu.com/s/16SRd_nx2r9p1y7SlaGj7qQ) 提取码：19n9
3.    《深入分析Java Web技术内幕》：[百度云链接](https://pan.baidu.com/s/1xW9nb9z3ae3WPbSJViPOuw) 提取码：b9ix
4.    《Java EE7权威指南卷2》：[百度云链接](https://pan.baidu.com/s/1KHQHZd7ChI9_H37Z-d_I5w) 提取码：6q51
5.    《JavaEE7精粹》：[百度云链接](https://pan.baidu.com/s/1LUMzUuCcTPQrcEDJGkraFg) 提取码：ej9e
6.  《轻量级Web应用开发》：[百度云链接](https://pan.baidu.com/s/1v-eUiWCyLdAY7wvdDh4_XA) 提取码：cugm
7.  《Web开发权威指南》：[百度云链接](https://pan.baidu.com/s/1ULE4PUBvyoaH6nPwunX-uQ) 提取码：t37j
8.  《Java EE互联网轻量级框架整合开发 SSM框架（Spring MVC+Spring+MyBatis）和Redis实现》：[百度云链接](https://pan.baidu.com/s/12GqqidZgmgdGehArMzsyjw) 提取码：86hm
9.  《Java Web企业项目实战》：[百度云链接](https://pan.baidu.com/s/1Rl4K3jUzDCxHjr5jy0mtWA) 提取码：38uf


### spring实战
0.    《Spring Data JPA从入门到精通》：[百度云链接](https://pan.baidu.com/s/1u7Gvdi5-12-IEk4OUyjbZQ) 提取码：nvfb
1.    《Spring MVC+ MyBatis快速开发与项目实战：[百度云链接](https://pan.baidu.com/s/1OVzFcNaiGF9WqzRmw0EdNw) 提取码：ncdd
2.    《Spring MVC+MyBatis开发从入门到项目实战：[百度云链接](https://pan.baidu.com/s/1bDz3-lE7WVGofahRwSqjUw) 提取码：dhfc
3.    《Spring5高级编程（第5版）》_王净译_18-12-01：[百度云链接](https://pan.baidu.com/s/1d2d-OtzfYkHHS0SEvrIdUA) 提取码：f7y7
4.    《精通Spring MVC4》：[百度云链接](https://pan.baidu.com/s/1Cr0qrAqTZKJLHFC2XjBOJg) 提取码：fc5f
5.    《精通Spring(清晰书签版)》：[百度云链接](https://pan.baidu.com/s/1ahInuei4HXpgOxekpH0_Jw) 提取码：kt6d
6.    《Java EE互联网轻量级框架整合开发 SSM框架（Spring MVC+Spring+MyBatis）和Redis实现》：[百度云链接](https://pan.baidu.com/s/1BjUXsPzbiIpfPxG5OpvcjA) 提取码：mmgv
7.    《Java EE设计模式：Spring企业级开发最佳实践》：[百度云链接](https://pan.baidu.com/s/1wSm8Y-9wU2xxfZ8nB-kj2g) 提取码：b4ra
8.    《Spring Batch批处理框架》：[百度云链接](https://pan.baidu.com/s/1tC06SVv2bQv8i7kFK3lhbA) 提取码：h5h4
10.    《Spring实战(第4版)》：[百度云链接](https://pan.baidu.com/s/1fdTAE3y9F6G7T5_zAs47IQ) 提取码：kniw


### spring源码
0.    《互联网轻量级SSM框架解密：Spring、Spring MVC、MyBatis源码深度剖析：[百度云链接](https://pan.baidu.com/s/1CzGno5Z7MWNZiDz0iTI2KA) 提取码：yn67
1.    《Spring源码深度解析（第2版）》_郝佳_2019-01-01：[百度云链接](https://pan.baidu.com/s/1TV0lGa_7i6irb25wY8uZLQ) 提取码：gmfv
2.    《SPRING技术内幕：深入解析SPRING架构与设计原理》：[百度云链接](https://pan.baidu.com/s/1BZg-oKxymIE8p7lEYVgcjg) 提取码：v1ms
3.    《spring揭秘(完整)》：[百度云链接](https://pan.baidu.com/s/13fge_tsE0vcLukpcCp1-NQ) 提取码：zzb6
4.    《Spring源码深度解析》：[百度云链接](https://pan.baidu.com/s/1o3VJ-nAa7JPVA5duNZ8tzg) 提取码：vvuy


### 并发编程
0.    《精通Java并发编程 第2版》_唐富年译_2018-10-01：[百度云链接](https://pan.baidu.com/s/1aDY0gkUcx6FoH-GC6v0V9A) 提取码：carr
1.    《实战Java高并发程序设计（第2版）》_葛一鸣_2018-10-11：[百度云链接](https://pan.baidu.com/s/1646YaeCbeu-jrQamPzmVeg) 提取码：iujj
2.    《Java并发编程：核心方法与框架》：[百度云链接](https://pan.baidu.com/s/1t8LRyxTqmqCZOGTSDFwAww) 提取码：gc95
3.    《Java并发编程之美》_翟陆续等_2018-10-01：[百度云链接](https://pan.baidu.com/s/1EiJYPk-Ui50uUuSjQIgdQg) 提取码：fiwg
4.    《Java多线程编程实战指南（核心篇）》：[百度云链接](https://pan.baidu.com/s/1eAGaNa9rPI12HRpUnyOz4A) 提取码：qgia
5.    《Java高并发编程详解-多线程与架构设计》_汪文君：[百度云链接](https://pan.baidu.com/s/1yeo4mr_zV58KeC7OAlQxaA) 提取码：tfmf
6.    《七周七并发模型_PDF电子书下载 带书签目录 高清完整版》：[百度云链接](https://pan.baidu.com/s/1-SvCZV2pz-RnL0mP8LYGYg) 提取码：7r3f
7.    《实战Java高并发程序设计》：[百度云链接](https://pan.baidu.com/s/1mI_PJoEn5cQ6eQt_0bfzrw) 提取码：kcht
8.    《图解Java多线程设计模式》：[百度云链接](https://pan.baidu.com/s/1G5dPYJqtwaQPKTLwHsXZ4Q) 提取码：xuhw
9.    《亿级流量网站架构核心技术 跟开涛学搭建高可用高并发系统》：[百度云链接](https://pan.baidu.com/s/1gw00uFmawK-9DkEhaXSjsw) 提取码：7b2j
11.    《Java并发编程：设计原则与模式（第二版）》：[百度云链接](https://pan.baidu.com/s/1wYuMxWmsYGSBMho7obxukw) 提取码：puzq
12.    《JAVA并发编程实践》：[百度云链接](https://pan.baidu.com/s/18KCfSNzeaJgYpkPkkkgmyA) 提取码：d8ek
13.    《java并发编程艺术》：[百度云链接](https://pan.baidu.com/s/1Ci7uenQ5jGueB0v6OpvYiw) 提取码：vc9g
15.    《Java多线程编程深入详解》：[百度云链接](https://pan.baidu.com/s/1TjfuvSv2RKMDlk77kkH33A) 提取码：4us7
16.    《Java多线程编程实战指南 设计模式篇》：[百度云链接](https://pan.baidu.com/s/19MvzgKgnmb6-L9ZmUIVQGw) 提取码：p8jr
17.    《java性能优化权威指南（带书签）》：[百度云链接](https://pan.baidu.com/s/1bTCKDPKqHal_37i6zoBcMg) 提取码：tc4a

### 计算机网络
0.    《计算机网络(第五版.谢希仁)》：[百度云链接](https://pan.baidu.com/s/1FFGZ75oA5vyJni0qN2VvkA) 提取码：5cbx
1.    《图解TCP IP(第5版)》：[百度云链接](https://pan.baidu.com/s/1HDZtzehYfWvbYBWiOXA_vg) 提取码：ejqz
2.    《HTTP权威指南》：[百度云链接](https://pan.baidu.com/s/1qlvAuRBLuaLdk0VFfli5HA) 提取码：n3a3
3.    《TCP-IP详解卷1：协议》：[百度云链接](https://pan.baidu.com/s/1lUsR62hP8Vb6aACDkftbNg) 提取码：pua5
4.    《TCP-IP详解卷2：实现》：[百度云链接](https://pan.baidu.com/s/1-hM1dyepPqvhUy7CTr0KZw) 提取码：sgh7
5.    《TCP-IP详解卷3：TCP事务协议，HTTP，NNTP和UNIX域协议》：[百度云链接](https://pan.baidu.com/s/19opFKLbEnOZRD4odwK2mEg) 提取码：npmc


### 网络编程
0.    《深入理解计算机网络》：[百度云链接](https://pan.baidu.com/s/1fF7m-qfpHiR0naDutaV6QQ) 提取码：ptsu
1.    《网络原理与应用》：[百度云链接](https://pan.baidu.com/s/1U42Gft4AN04Jmd63Q7hTMA) 提取码：c4q6
2.    《HTTP2基础教程》：[百度云链接](https://pan.baidu.com/s/1fyJHSw9tsOn6_JILa7JUWw) 提取码：tn8m
3.    《NIO与Socket编程技术指南》_高洪岩：[百度云链接](https://pan.baidu.com/s/1FWuXDzWk7JWIT2-bc1FE_w) 提取码：m56t
4.    《TCPIP详解卷2》：[百度云链接](https://pan.baidu.com/s/1bCDkDw9whlqMcXjaxEuBPg) 提取码：jbrb
5.    《TCPIP详解卷3》：[百度云链接](https://pan.baidu.com/s/1wQtd07TZsDdqBuGZHGWeGg) 提取码：625q


### WEB开发
0.    《高性能网站建设进阶指南》：[百度云链接](https://pan.baidu.com/s/1MZfXOT7nFhtc24QhCxwPBA) 提取码：28mg
1.    《论道HTML5》：[百度云链接](https://pan.baidu.com/s/1l1oLVVBAkW23b07PZgZMnQ) 提取码：ix8w
2.    《认识与设计：理解UI设计准则》：[百度云链接](https://pan.baidu.com/s/1yNuYH0HDEwF4NqIx_ULNDQ) 提取码：3en8
3.    《CSS权威指南(第3版)》：[百度云链接](https://pan.baidu.com/s/1ekjzj8DFXw3cj2rrqAAuUg) 提取码：e6zz
4.    《HTML5程序设计(第2版)》：[百度云链接](https://pan.baidu.com/s/1tn7xSMtawgYFiwQtPoszpA) 提取码：n5qc
5.    《HTML5权威指南》：[百度云链接](https://pan.baidu.com/s/1ktlsz0sZn4nXFRc-IQPdKA) 提取码：herg
6.    《JavaScript高级应用与实践》：[百度云链接](https://pan.baidu.com/s/1y-tFrz8Hpta6-8GH5mqEIA) 提取码：ygrt
7.    《JavaScript权威指南(第4版)》：[百度云链接](https://pan.baidu.com/s/1JPW94IgffLZpK6Y44WWUHw) 提取码：tghe
8.    《JavaScript入门经典(第4版)》：[百度云链接](https://pan.baidu.com/s/1W6hOrbV6lzJXPaA7q80xUA) 提取码：yfw8
9.    《JavaScript权威指南(第6版)》：[百度云链接](https://pan.baidu.com/s/1Ukv4a9kyymp7_EqqQkEZIw) 提取码：gwa4
10.    《jQuery权威指南》：[百度云链接](https://pan.baidu.com/s/17Nhx3V5KGfi1yPw0CYowHw) 提取码：tz8r
11.    《WebKit技术内幕》：[百度云链接](https://pan.baidu.com/s/1X3tTi6njgJBsWbG4nOfmig) 提取码：uw5h



### 数据结构与算法
0.    《啊哈！算法》：[百度云链接](https://pan.baidu.com/s/10CDSjQU_VA4pcUqrN5-s1A) 提取码：8dni
1.    《数据结构与算法经典问题解析：Java语言描述(原书第2版）》：[百度云链接](https://pan.baidu.com/s/1VARZCtQgkWjyoBtgcUi8Fw) 提取码：7bqv
2.    《算法（第4版）》_谢路云译_2012-10-01：[百度云链接](https://pan.baidu.com/s/19XdvXWWTY0n_zOdiBZ_9Fg) 提取码：ks3t
3.    《算法笔记》：[百度云链接](https://pan.baidu.com/s/1NpnzdjnEsObNQfkLLrvKbg) 提取码：qegg
4.    《算法详解 卷1 算法基础》_徐波译_2019-01-01：[百度云链接](https://pan.baidu.com/s/1aKnKWqUcyMk6VlHdy66rPA) 提取码：dvfp
5.    《图解算法》：[百度云链接](https://pan.baidu.com/s/1MTLLmVXscTwlDVWGP7P3eQ) 提取码：7evu
6.    《啊哈！算法》：[百度云链接](https://pan.baidu.com/s/1xNii9jn2Jcc9MPG54Jyr8A) 提取码：9u4k
7.    《编程之法面试和算法心得》：[百度云链接](https://pan.baidu.com/s/1nouBQaxXQMiReLDZvFDVZQ) 提取码：z6j9
8.    《编程之美》：[百度云链接](https://pan.baidu.com/s/1dDV8AJmlrfFQ2nTgFjr-Jg) 提取码：wp9v
9.    《编程珠玑(第2版)》：[百度云链接](https://pan.baidu.com/s/1c04KKqHiUoXuglywyDfeEA) 提取码：cng1
10.    《编程珠玑2》：[百度云链接](https://pan.baidu.com/s/13JEwC_onTY0VDKP_NKpJBA) 提取码：ampq
11.    《程序员代码面试指南 IT名企算法与数据结构题目最优解 （左程云著）》：[百度云链接](https://pan.baidu.com/s/1lY2ehL6gsz1Uo-K-a3yxBg) 提取码：6vy1
12.    《程序员的数学3+线性代数》：[百度云链接](https://pan.baidu.com/s/1cRbrrNL9gzyuxtGXxJ7YHw) 提取码：fgaa
13.    《程序员面试金典（第5版）》：[百度云链接](https://pan.baidu.com/s/1n91BmEhcHd_zvrLAj85h1A) 提取码：bdke
14.    《大话数据结构》：[百度云链接](https://pan.baidu.com/s/1fxAfmZkwDD9o9AuqlktRKQ) 提取码：bvfn
15.    《计算机程序设计艺术第1卷：基本算法（第3版）》：[百度云链接](https://pan.baidu.com/s/1dSaPUb_RWxOc17gkAZHCTg) 提取码：fbuj
16.    《计算机程序设计艺术第2卷：半数值算法（第3版）》：[百度云链接](https://pan.baidu.com/s/1uMhHnWesVedbnU_gkLT-YA) 提取码：qr8f
17.    《计算机程序设计艺术第3卷：排序与查找（第2版）》：[百度云链接](https://pan.baidu.com/s/1NMfw8LsMr8TK_97mEetoHQ) 提取码：jssb
18.    《剑指offer》：[百度云链接](https://pan.baidu.com/s/1LfgI6UhH_QbV1VOqS83kGA) 提取码：db26
19.    《剑指OFFER(第2版)》：[百度云链接](https://pan.baidu.com/s/1XqdTgifqRe__9T-3452jPQ) 提取码：aedd
20.    《剑指Offer(纪念版)》：[百度云链接](https://pan.baidu.com/s/14myuc2Zpq7nOBrTqYVJZ5Q) 提取码：zy3t
21.    《妙趣横生的算法C语言实现》：[百度云链接](https://pan.baidu.com/s/1PL2bVQO2DW8ATedlERfeRw) 提取码：yncg
22.    《数据结构(C语言版).严蔚敏_吴伟民.扫描版》：[百度云链接](https://pan.baidu.com/s/1toM6MnAv7fa3W92R237RaA) 提取码：w76b
23.    《数据结构（Java版）》：[百度云链接](https://pan.baidu.com/s/1AfqTStGU6__-yZtNcCjfEw) 提取码：r233
24.    《数据结构与算法分析_Java语言描述(第2版)》：[百度云链接](https://pan.baidu.com/s/1_05zIDduZFpgNnA4IJ6uNA) 提取码：8cfi
25.    《数据结构与算法分析(C++描述)(第3版)》：[百度云链接](https://pan.baidu.com/s/10fHaVuRIBgF03hiJrEYDZg) 提取码：rih8
26.    《算法导论(第2版)》：[百度云链接](https://pan.baidu.com/s/1xQwfqHpJ8yysyrP6BJejqQ) 提取码：xsyx
27.    《算法导论(原书第3版) 中文完整版 高清扫描版》：[百度云链接](https://pan.baidu.com/s/1rx9XIbj-BO_k48VXGQ6vGw) 提取码：bygx
28.    《算法基础.打开算法之门》：[百度云链接](https://pan.baidu.com/s/1-avjOacBD8SwuEU5entjag) 提取码：6cuf



### 高阶编程
0.    《编码—隐匿在计算机软硬件背后的语言上》：[百度云链接](https://pan.baidu.com/s/1QOesXZ9-SD1TPzDph0Ki4g) 提取码：t5ib
1.    《编码的奥秘》：[百度云链接](https://pan.baidu.com/s/1nhEnWCua46NAWepVAX6D0g) 提取码：q83p
2.    《程序员的自我修养—链接、装载与库》：[百度云链接](https://pan.baidu.com/s/1MjFFXVtNiF2iP9QVnN4M6g) 提取码：49cq
3.    《程序员修炼之道》：[百度云链接](https://pan.baidu.com/s/1-kU1xOPkVpMViyY8SGkX4w) 提取码：d4z4
4.    《代码整洁之道》：[百度云链接](https://pan.baidu.com/s/1P-w9IaHR9Kzp3qguG_5TCA) 提取码：tsw7
5.    《高效能人士的七个习惯》：[百度云链接](https://pan.baidu.com/s/1uv2LW4aGYEMYFQoRdWi4Vw) 提取码：mw97
6.    《计算机程序的构造和解释》：[百度云链接](https://pan.baidu.com/s/1KWyxT8schAdVqIz1rpnPnA) 提取码：ejun
7.    《浪潮之巅》：[百度云链接](https://pan.baidu.com/s/1diiu5yTnXPlk9-Pm2h3RQQ) 提取码：fu41
8.    《全栈增长工程师指南》：[百度云链接](https://pan.baidu.com/s/1JCQbObKepQlR_DO5_Io7aQ) 提取码：u8i1
9.    《人月神话》：[百度云链接](https://pan.baidu.com/s/1t0RudonS9jAbCUqhHLMOjw) 提取码：j6kf
10.    《数学之美(第2版)》：[百度云链接](https://pan.baidu.com/s/1ZURs4OrYgl_Pj6gBYXikig) 提取码：2crj
11.    《数学之美(第二版)》：[百度云链接](https://pan.baidu.com/s/1pmljM1BuPhlDoz7aqWpVLA) 提取码：q1wp
12.    《信息检索导论》：[百度云链接](https://pan.baidu.com/s/1e7tSl4cOaiHay1XvuEvQmA) 提取码：i7d9
13.    《修改代码的艺术》：[百度云链接](https://pan.baidu.com/s/1feTz-W5E3g9Pnxp46ZMvSw) 提取码：u5rd
14.    《一万小时天才理论》：[百度云链接](https://pan.baidu.com/s/1zpiZRFWOdXI0QggghoLsUw) 提取码：8n3h





### MySQL
0.    《高性能MySQL(第2版)》：[百度云链接](https://pan.baidu.com/s/1IVQq0d2ECE11wD7SCgmVSw) 提取码：x3gc
1.    《高性能MySQL(第3版)》：[百度云链接](https://pan.baidu.com/s/1d_xWH-aG73yhpxcd-JHBdg) 提取码：7xwa
2.    《MySQL5权威指南（第3版）》：[百度云链接](https://pan.baidu.com/s/1feadtM24wjJ7UyOuN8ai9Q) 提取码：gug9
3.    《MYSQL必知必会》：[百度云链接](https://pan.baidu.com/s/1FeZtpypLA-WbSPG15mar5Q) 提取码：6m5r
4.    《MySQL技术内幕：SQL编程》：[百度云链接](https://pan.baidu.com/s/1Kj6qmv-Yu-xu1LfNuc1aIg) 提取码：ims2
5.    《MySQL技术内幕(第4版)》：[百度云链接](https://pan.baidu.com/s/1_kqRJ6NC3FCG78MC3-xGsQ) 提取码：yegm
6.    《MySQL技术内幕InnoDB存储引擎》：[百度云链接](https://pan.baidu.com/s/1I_cDdigLoAsfvPruE3gMaw) 提取码：jeg4
7.    《MySQL性能调优与架构设计》：[百度云链接](https://pan.baidu.com/s/1AcbshEydmu-boDlmfSt99A) 提取码：wi6d


### Oracle
0.    《编程艺术深入数据库体系结构》：[百度云链接](https://pan.baidu.com/s/10bBoaWJtn-7PQbGfC8RQgA) 提取码：ta3f
1.    《收获，不止Oracle》：[百度云链接](https://pan.baidu.com/s/1vOclijwKXMunGe312GpAIw) 提取码：h3kx
2.    《PLSQL操作手册》：[百度云链接](https://pan.baidu.com/s/1_WE6QRou3tQgl_KNuFnjOw) 提取码：5t7p



### MongoDB
0.    《MongoDB运维实战》_张甦等_2018-09-01：[百度云链接](https://pan.baidu.com/s/1EsIt4V4-IULLkdb_LGUzdQ) 提取码：h5ca
1.    《深入学习MongoDb》：[百度云链接](https://pan.baidu.com/s/1bsr1HooXdeUYw1cheUW_lA) 提取码：wjfp
2.    《MongoDB权威指南第2版》：[百度云链接](https://pan.baidu.com/s/1qnuNjq2DKMXfiYDkFcjqkg) 提取码：b3it
3.    《MongoDB实战》：[百度云链接](https://pan.baidu.com/s/1xd7odbRul_d9RgrhK7hJ4w) 提取码：pdp6



### SQL Server
1.    《SQL2005技术内幕： T-SQL程序设计》：[百度云链接](https://pan.baidu.com/s/1FP1aPsJv5i0yLsTLGtGTqg) 提取码：c8x4
2.    《SQL2005技术内幕：存储引擎》：[百度云链接](https://pan.baidu.com/s/1JyhQkxdpDC9I-lchfj4Y1A) 提取码：281d
3.    《SQL2008技术内幕：T-SQL查询》：[百度云链接](https://pan.baidu.com/s/1WnLVpTJkvF5Hh8JRiYi8Xw) 提取码：4ffc
4.    《SQL2008技术内幕：T-SQL语言基础》：[百度云链接](https://pan.baidu.com/s/150-lrOiaMfw1G8FxEtW8KA) 提取码：u7ke
5.    《SQL反模式》：[百度云链接](https://pan.baidu.com/s/1qIYduP5_qtK0zFvLrEMBgQ) 提取码：194d
6.    《Transact-SQL权威指南》：[百度云链接](https://pan.baidu.com/s/1mnHCdUtTrieUMvVLOCqeeA) 提取码：h9v7


### 操作系统
0.    《高级Bash脚本编程指南.3.9.1 (杨春敏 黄毅 译)》：[百度云链接](https://pan.baidu.com/s/1Qt_VZ9YVVYsvSHPtODpXcA) 提取码：xdvg
1.    《汇编语言(第3版.王爽)》：[百度云链接](https://pan.baidu.com/s/1enLHZ0yENJHjlMXUGYoeTg) 提取码：gxy7
2.    《计算机操作系统(第3版.汤子瀛)》：[百度云链接](https://pan.baidu.com/s/1iRJwsTh7UqeptAEd2VE3lQ) 提取码：ptdp
3.    《计算机组成原理(白中英)》：[百度云链接](https://pan.baidu.com/s/1GSmrmg0_1DWoSeRhy8s8Tg) 提取码：rne2
4.    《计算机组成原理(第2版.唐朔飞)》：[百度云链接](https://pan.baidu.com/s/1W25jiaUCyMn1v3LhS4-YMQ) 提取码：dpvr
5.    《鸟哥的Linux私房菜基础篇(第3版)》：[百度云链接](https://pan.baidu.com/s/1gZOIPfi8pFnti57Nnk8wcQ) 提取码：v9w1
6.    《深入理解计算机系统》：[百度云链接](https://pan.baidu.com/s/1Zbgy1ri753Rg95N938aSSQ) 提取码：kpa2
7.    《Linux高性能服务器编程》：[百度云链接](https://pan.baidu.com/s/13T5Cc1OzKMzGce3BWVm3PA) 提取码：ef81
8.    《Shell脚本学习指南》：[百度云链接](https://pan.baidu.com/s/1tACjwgSU_5UBKn24dl4aCg) 提取码：h4yw
9.    《UNIX网络编程卷1：套接字联网API（第3版）》：[百度云链接](https://pan.baidu.com/s/1hj8MFKELPkCYXDZ-vaUpew) 提取码：c5j4



### 工作流引擎
0.    《Activiti权威指南》：[百度云链接](https://pan.baidu.com/s/1-U_9pBqqzqIAPForXbS9SQ) 提取码：tq9s


### 机器学习
0.    《机器学习实战：基于Scikit-Learn、Keras和TensorFlow》(第2版)：[百度云链接](https://pan.baidu.com/s/1gKBzzSQsrGsb84OKp3EKYg) 提取码：ajsy
1.    《概率论及其应用》：[百度云链接](https://pan.baidu.com/s/1JmqYFZnFtmo5-lC-l3FCtQ) 提取码：mn63
2.    《概率论及其应用(3rd 威廉费勒)》：[百度云链接](https://pan.baidu.com/s/1Rubvrt2Yi4x4ey9KbJXrKg) 提取码：idyw
3.    《机器学习（西瓜书.周志华）》：[百度云链接](https://pan.baidu.com/s/1cBRok8zDtTP2qjwTSJg0iA) 提取码：n83j
4.    《机器学习方法》：[百度云链接](https://pan.baidu.com/s/1SUHTqS0inGcuZ4K1ps5HOQ) 提取码：k9ri
5.    《机器学习实战》：[百度云链接](https://pan.baidu.com/s/1W3GzseyXVTfQCr3yOwXQjA) 提取码：7u68
6.    《集体智慧编程》：[百度云链接](https://pan.baidu.com/s/1XpCbXbF8T0I3vBYJP9lDqg) 提取码：e5wb
7.    《人工智能：一种现代方法（第2版）部分1》：[百度云链接](https://pan.baidu.com/s/1hJCKLzCn5K41PTVCnGkysg) 提取码：tde5
8.    《人工智能：一种现代方法（第2版）部分2》：[百度云链接](https://pan.baidu.com/s/1YZDaLWNaY2P8e0UlixYBVg) 提取码：jg5s
9.    《社交网站的数据挖掘与分析》：[百度云链接](https://pan.baidu.com/s/1b5BmDvNbPz3vTHIO3z8MMA) 提取码：hkfi
10.    《神经网络与深度学习(邱锡鹏)》：[百度云链接](https://pan.baidu.com/s/1nTcXeToQ9d3xz6pwnDoiNA) 提取码：2y9h
11.    《统计学习方法》：[百度云链接](https://pan.baidu.com/s/1CfNIXzgYaM1OBA-D998SVw) 提取码：pvpf
12.    《deepLearning深度学习(开源版)》：[百度云链接](https://pan.baidu.com/s/1btO_5QogyM6zr6qylxEvsA) 提取码：n3ph
13.    《PRML_Pattern Recognition And Machine Learning - Springer  2006》：[百度云链接](https://pan.baidu.com/s/1KG68LARNS-YJkVML49oTcA) 提取码：41jt
14.    《PRML_Translation》：[百度云链接](https://pan.baidu.com/s/1q0B_4UmTmeUpFtmHTQCIfg) 提取码：5esq
15.    《PRML习题答案完整版》：[百度云链接](https://pan.baidu.com/s/1xVNtLNLG1a59oZZZ3L5myQ) 提取码：grtp
16.    《TensorFlow 2.0深度学习算法实战》：[百度云链接](https://pan.baidu.com/s/1DsgrMqX9eDbXHOAV_nUGzQ) 提取码：3v9f
17.    《WEKA应用技术与实践》：[百度云链接](https://pan.baidu.com/s/1av6D_wVOiGxxcsHzfzDb3A) 提取码：itzs





### 大数据
0.    《大数据处理系统Hadoop源代码情景分析》：[百度云链接](https://pan.baidu.com/s/1hBGQ74rIw9J8nv6F6Tw88Q) 提取码：qyxe
1.    《大数据架构师指南》：[百度云链接](https://pan.baidu.com/s/1FWDkmZn68SG_PiYn5IUi7Q) 提取码：ssc9
2.    《大数据架构详解：从数据获取到深度学习》：[百度云链接](https://pan.baidu.com/s/12Hljx-3socGd_uBuR7ZisQ) 提取码：ijmc
4.    《架构大数据：大数据技术及算法解析》：[百度云链接](https://pan.baidu.com/s/1WszjeI0w0_ROZVXl9KP_VQ) 提取码：vn65
5.    《轻松学大数据挖掘：算法、场景与数据产品》：[百度云链接](https://pan.baidu.com/s/1XRVcXduzaMQuLuxSgqZQ6w) 提取码：5dm2
6.    《深入理解大数据：大数据处理与编程实践》：[百度云链接](https://pan.baidu.com/s/1toKh7QqBqzm9vwVxGZnLXw) 提取码：udqn
7.    《实战Hadoop2.0（第二版――从云计算到大数据》：[百度云链接](https://pan.baidu.com/s/10kYdwR074JUdP4aWFKaCTQ) 提取码：tipk
8.    《智慧城市：大数据、物联网和云计算之应用》：[百度云链接](https://pan.baidu.com/s/1ZsYwhp8pxiON_UyiaEJNGw) 提取码：cthv
9.    《Elasticsearch集成Hadoop最佳实践》：[百度云链接](https://pan.baidu.com/s/1vCceB3rMD5mXUZ2NKJqvYA) 提取码：w6vi
10.    《Hadoop+Spark大数据巨量分析与机器学习整合开发实战》：[百度云链接](https://pan.baidu.com/s/1AHchPM-N4UUhd_pYHiI8ew) 提取码：eptm
11.    《Hadoop2.X_HDFS源码剖析》：[百度云链接](https://pan.baidu.com/s/1WHkQ6ekUsP9Cw9_1h_LHoQ) 提取码：k9wm
12.    《Hadoop大数据开发案例教程与项目实战》：[百度云链接](https://pan.baidu.com/s/1K1wtWX7g6SNfhFqPFkoXEA) 提取码：92ng
13.    《Hadoop大数据实战权威指南》：[百度云链接](https://pan.baidu.com/s/1oIq6uySnYr43eq7Rzl5KnA) 提取码：e2sm
14.    《HBase不睡觉书_杨曦_2018-01-01：[百度云链接](https://pan.baidu.com/s/1byqWrnKEWYF23aDLxD66KQ) 提取码：hpsj
15.    《HBase入门与实践_彭旭_2018-12-01：[百度云链接](https://pan.baidu.com/s/1x7OXP8cDrnjIzg3b5zvQcA) 提取码：tr5q
16.    《HBase应用架构》：[百度云链接](https://pan.baidu.com/s/17lTQMIABMfn1AQQxxPw2tA) 提取码：m5et
17.    《Spark大数据商业实战三部曲：内核解密商业案例性能调优》：[百度云链接](https://pan.baidu.com/s/19dXdeFdQItVFhMk-qmKIoQ) 提取码：twtx
18.    《白话大数据与机器学习_章节目录》：[百度云链接](https://pan.baidu.com/s/11qFyTxmf3_sHePP7Rc2s9Q) 提取码：n3ft
19.    《大数据架构师指南》：[百度云链接](https://pan.baidu.com/s/1FjL1Ny1zoCEimaGiZcas4Q) 提取码：suk6
20.    《大数据架构详解：从数据获取到深度学习》：[百度云链接](https://pan.baidu.com/s/1Tupbh0oSXgbrJhebt6EMBw) 提取码：khgv
21.    《大数据挖掘：系统方法与实例分析》：[百度云链接](https://pan.baidu.com/s/1rCAtnPJ-koYqqUmPIxcKOA) 提取码：x334
22.    《数据算  法 Hadoop Spark大数据 处理技巧》：[百度云链接](https://pan.baidu.com/s/1mMbOJvTTDwWla3KqMvnD-g) 提取码：ftha
23.    《数据挖掘概念与技术(第3版)》：[百度云链接](https://pan.baidu.com/s/1PR9SCDKF6hGguSZkEo6UMQ) 提取码：h4tf
24.    《用户网络行为画像_大数据中的用户网络行为画像分析与内容推荐应用》：[百度云链接](https://pan.baidu.com/s/1SziGPMc8bSX3rptoiTZbQw) 提取码：jug5
25.    《自己动手做大数据系统.张魁(带书签文字版)》：[百度云链接](https://pan.baidu.com/s/1nfKN-6vZZ9kf5kFeihBRIg) 提取码：a7cj
26.    《Druid实时大数据分析原理与实践》：[百度云链接](https://pan.baidu.com/s/1mMbWyqbWrNNiHZ0-tEqWZA) 提取码：gk19
27.    《Hadoop大数据处理技术基础与实践》：[百度云链接](https://pan.baidu.com/s/1s7c8LSb6TgdXgKnquEKTgA) 提取码：kcys
28.    《Hadoop大数据分析与挖掘实战》：[百度云链接](https://pan.baidu.com/s/1lBiEG5UMMCqDB0PN_-WCjQ) 提取码：a6wg
29.    《hadoop简介》：[百度云链接](https://pan.baidu.com/s/11_CjdMFYoLsWKF4Wq6B9SA) 提取码：ipxe
30.    《Hadoop权威指南.大数据的存储与分析.第4版.修订版&升级版》：[百度云链接](https://pan.baidu.com/s/1jR1MMrXzio_yc8a65PB6Kw) 提取码：zqeb
31.    《Hadoop权威指南(第2版)》：[百度云链接](https://pan.baidu.com/s/1H86xOgcIFBtSNInlP0LOhw) 提取码：dmg6
32.    《Hadoop权威指南（中文第2版）》：[百度云链接](https://pan.baidu.com/s/1gsVgBQwQESy0N5AtMvUMcw) 提取码：ymdm
33.    《HBase实战》：[百度云链接](https://pan.baidu.com/s/1bNNGhw8c0hOak9CEQfFx_w) 提取码：ida8



### 大数据相关
0.    《数据挖掘概念与技术(第3版)》：[百度云链接](https://pan.baidu.com/s/1ChWNd9KZGTWoweV1RqmgWQ) 提取码：cgk9
1.    《Hadoop大数据处理技术基础与实践》：[百度云链接](https://pan.baidu.com/s/1iWK2F2NCVGvoUgoc7poc3g) 提取码：xvyg
2.    《Hadoop权威指南(第2版)》：[百度云链接](https://pan.baidu.com/s/1ApF43fy_04elr87Sq26W7w) 提取码：g4ph
3.    《MongoDB权威指南》：[百度云链接](https://pan.baidu.com/s/1v_wgzf_7tvROm85PAS2qsQ) 提取码：fapk
4.    《MongoDB实战》：[百度云链接](https://pan.baidu.com/s/1YLr6--8ZBtNtgYVYx6S01Q) 提取码：ab3s
5.    《PySpark实战指南》：[百度云链接](https://pan.baidu.com/s/1JHolhe2y-X53_yhiE791PA) 提取码：v5wy
6.    《R与Hadoop大数据分析实战》：[百度云链接](https://pan.baidu.com/s/1_JCy2TuZ7SnI_zGHjo9psA) 提取码：8tci
7.    《Redis实战》：[百度云链接](https://pan.baidu.com/s/1P8ZCrY3E_-fckyyk7BOxYQ) 提取码：cj44
8.    《Spark大数据处理：技术、应用与性能优化》：[百度云链接](https://pan.baidu.com/s/1ASsOK8xm0GF1h3tZ3SnSOg) 提取码：cygq
9.    《Spark快速数据处理》：[百度云链接](https://pan.baidu.com/s/1J27QE2gl7doQ-4FOtkZRTA) 提取码：4a6d


### 分布式
0.    《深入分布式缓存 从原理到实践》：[百度云链接](https://pan.baidu.com/s/12PwfhPMOSA5z5vXMssTYBg) 提取码：g3ny
1.    《从Paxos到Zookeeper_分布式一致性原理与实践（书签版）》：[百度云链接](https://pan.baidu.com/s/10VVyITgtWh2YbY8iKs4U0w) 提取码：v9ag



### 安全相关
0.    《安全之路：Web渗透技术及实战案例解析（第2版）》_陈小兵_2015-09-01：[百度云链接](https://pan.baidu.com/s/1T6gx2RLWy_Ke0gz5Z22ZwQ) 提取码：y2qq
1.    《Web安全防护指南-基础篇》_蔡晶晶等_2018-05-01：[百度云链接](https://pan.baidu.com/s/14Ol9gSqEyhPQR7-GpQfLlw) 提取码：fj35
2.    《Web安全攻防：渗透测试实战指南》_李文轩等：[百度云链接](https://pan.baidu.com/s/15f0ndFKU8vgkZeQTXZTCqg) 提取码：r8mn
3.    《HTTPS权威指南在服务器和Web应用上部署SSL&TLS和PKI》：[百度云链接](https://pan.baidu.com/s/1sohYi9DM1FG6gC0r-bMUEw) 提取码：fdgm
4.    《java加密与解密艺术–完整版》：[百度云链接](https://pan.baidu.com/s/13ju8y4jWUltCqggmWXIS6w) 提取码：ixu4
5.    《Web安全开发指南》：[百度云链接](https://pan.baidu.com/s/1iBSS3TiwjEeGvj5U3JWP0A) 提取码：z3m4


### 架构设计
0.    《大型网站性能优化实战从前端网络CDN到后端大促的全链路性能优化》_周涛明_2019-01-01：[百度云链接](https://pan.baidu.com/s/11n70nUEXw8KUmjwhMz8xAw) 提取码：7ie4
1.    《大型网站系统与JAVA中间件实践（高清版）》：[百度云链接](https://pan.baidu.com/s/16Ra0_YE_YLCw_lhkS0_G8A) 提取码：8ujk
2.    《构建高性能WEB站点》：[百度云链接](https://pan.baidu.com/s/1ycpXNdp8PGIZb37GatOu0w) 提取码：qq5s
3.    《精通.NET企业项目开发：最新的模式、工具与方法》：[百度云链接](https://pan.baidu.com/s/1HwGFih2Xf91NFE7dxAvYWg) 提取码：cdk5
4.    《领域驱动设计--软件核心复杂性应对之道》：[百度云链接](https://pan.baidu.com/s/1p9VhpypJVyuiRWwFrN2UPg) 提取码：un2p
5.    《领域驱动设计与模式实战》：[百度云链接](https://pan.baidu.com/s/1lwJgCIYOntaTV6DzSKIEOA) 提取码：uyyc
7.    《企业应用架构模式》：[百度云链接](https://pan.baidu.com/s/1-UVbu2tKpaBjDPKwm_GmrQ) 提取码：sh67
8.    《探索CQRS和事件源》：[百度云链接](https://pan.baidu.com/s/1yFKVCEDJoLf3pIx4nTAvEw) 提取码：tf38
9.    《微服务架构与实践(王磊著)完整版》：[百度云链接](https://pan.baidu.com/s/1pI6bZqvpGLLQFL0Srj7R-w) 提取码：2e36
10.    《微软应用技术架构(第2版)》：[百度云链接](https://pan.baidu.com/s/10F18IaMNKGhLP4aN6rVKpw) 提取码：equd
11.    《重构_改善既有代码的设计》：[百度云链接](https://pan.baidu.com/s/1L5DwH2dEBQpoaGMHNkac0Q) 提取码：ab7e
12.    《重构与模式》：[百度云链接](https://pan.baidu.com/s/1wLzjH-TjpDZIXux-_Et7tA) 提取码：i29c
13.    《GOF设计模式》：[百度云链接](https://pan.baidu.com/s/1foKOyApeuvdWgYtYa-qkyw) 提取码：b3av
14.    《UML和模式应用(第3版)》：[百度云链接](https://pan.baidu.com/s/1FQEkhM0qB2CXkl7yo-BnAg) 提取码：7kvt


### 敏捷开发
0.    《测试驱动开发的3项修炼：走出TDD丛林》：[百度云链接](https://pan.baidu.com/s/1dqm6PGuvjH2IjD8tMBgUfQ) 提取码：4s4u
1.    《大规模定制模式下的敏捷产品开发》：[百度云链接](https://pan.baidu.com/s/19ln0wlsaRo0ayhRLnfMqDw) 提取码：x5uq
2.    《高效程序员的45个习惯：敏捷开发修炼之道》：[百度云链接](https://pan.baidu.com/s/1HNAiqo2fl-mu8Tcn3JeDZQ) 提取码：8t29
3.    《敏捷估计与规划》：[百度云链接](https://pan.baidu.com/s/1CXRrzfJb_ek2hAXej3TozA) 提取码：a7i2
4.    《敏捷技能修炼-敏捷软件开发与设计的最佳实践》：[百度云链接](https://pan.baidu.com/s/1ZNb3uFhw8thAoj6rCQ1QyA) 提取码：em42
6.    《敏捷开发的必要技巧》：[百度云链接](https://pan.baidu.com/s/16ONNMElJEDq5SHE1ZBbWtA) 提取码：shiq
7.    《敏捷开发的艺术》：[百度云链接](https://pan.baidu.com/s/11wLYeGCUx0HotO02e7y0ZA) 提取码：ppsh
8.    《敏捷开发回顾：使团队更强大》：[百度云链接](https://pan.baidu.com/s/1kwjxZnRoSHU3Jw-HJWvECw) 提取码：hr2m
9.    《敏捷开发知识体系》：[百度云链接](https://pan.baidu.com/s/1LjfTx1OJeJaf78OvJRd_AA) 提取码：86ky
10.    《敏捷软件开发：原则、模式与实践》：[百度云链接](https://pan.baidu.com/s/1L3KEsLPCoE45OD7sU_pFEQ) 提取码：ycv2
11.    《敏捷软件开发：原则、模式与实践(C#版)》：[百度云链接](https://pan.baidu.com/s/1AMCM_hFYCUqHKHDMQd1m6A) 提取码：cesu
12.    《敏捷无敌》：[百度云链接](https://pan.baidu.com/s/1GCN56TUEis9anoBkz1a0OQ) 提取码：4jft
13.    《敏捷武士：看敏捷高手交付卓越软件》：[百度云链接](https://pan.baidu.com/s/1b2abq0MBXzG6EvxQdLxHaQ) 提取码：g635
14.    《敏捷整合开发：更快改进性能的案例与实用技术》：[百度云链接](https://pan.baidu.com/s/170BzMVD8vdMa8H-vKpdfWQ) 提取码：s9y3
15.    《硝烟中的Scrum和XP》：[百度云链接](https://pan.baidu.com/s/1bC0emXwMqKlKJML8LYLHjg) 提取码：32px
16.    《应用Rails进行敏捷Web开发(第3版)》：[百度云链接](https://pan.baidu.com/s/1J2bT-A2s0av4LZbKQT_oIw) 提取码：gyk2
17.    《Scrum敏捷软件开发》：[百度云链接](https://pan.baidu.com/s/1dHMsp8b73QZ4W9W5A4s7RQ) 提取码：wgyk
18.    《Web开发敏捷之道：应用Rails进行敏捷Web开发（第4版）》：[百度云链接](https://pan.baidu.com/s/1ByRm-rTAggN7KqfmyvVaiQ) 提取码：pvfc
19.    《Web开发敏捷之道》：[百度云链接](https://pan.baidu.com/s/1x8ZiZ5P4CN2_PQko2mnG7w) 提取码：zp95



### 项目管理
0.    《成为技术领导者 掌握全面解决问题的方法》：[百度云链接](https://pan.baidu.com/s/1Wf3KVoQixp0Z5jf-Uo-NYA) 提取码：s5kt
1.    《程序员之禅》：[百度云链接](https://pan.baidu.com/s/1_QUi6A1B9qdZKrCECXPMSg) 提取码：6x2w
2.    《从技术走向管理――李元芳履职记（第2版）》：[百度云链接](https://pan.baidu.com/s/1Yg9hxF_WVyv3cOW82g7dJA) 提取码：fz8i
3.    《互联网项目管理实践精粹》：[百度云链接](https://pan.baidu.com/s/1Q4jPu7XJ8hi1Ko_itARZgw) 提取码：rwbt
4.    《技术管理之巅—如何从零打造高质效互联网技术团队》：[百度云链接](https://pan.baidu.com/s/16kflgjcLUpayS6k6pPi-mw) 提取码：qx6j
5.    《技术领导力：程序员如何才能带团队》：[百度云链接](https://pan.baidu.com/s/1ufWZyftJWm67hTctJJrXig) 提取码：j64i
6.    《技术之瞳 阿里巴巴技术笔试心得》：[百度云链接](https://pan.baidu.com/s/1W1Y0hZJ920LFjbKtVzmgQg) 提取码：rd13
7.    《尽在双11 阿里巴巴技术演进与超越》：[百度云链接](https://pan.baidu.com/s/1utYSqyKymHkN3fzh9xcTKw) 提取码：redr
8.    《领导团队的智慧(中高层管理者的9项管理实践)--附赠手册》：[百度云链接](https://pan.baidu.com/s/1oAKNFkZUmdHBKsE6HieQMQ) 提取码：3wjd
10.    《快速转行做产品经理》_李三科_2018-07-01：[百度云链接](https://pan.baidu.com/s/1Ge_jK98lAnj2qMOBeX1TbQ) 提取码：fusa
11.    《破茧成蝶2--以产品为中心的设计革命》_刘津等_2018-08-01：[百度云链接](https://pan.baidu.com/s/1dzKrZcsgqERtxUqCzpYPeA) 提取码：k7zf
12.    《人人都是产品经理2.0》_苏杰：[百度云链接](https://pan.baidu.com/s/1u3kwkUMcRQ0U3uMKOdUWgQ) 提取码：dy4h
13.    《数据产品经理必修课：从零经验到令人惊艳》_李鑫：[百度云链接](https://pan.baidu.com/s/1TZnRFXmJR6In4lLcJQh48g) 提取码：hxm3
14.    《突破——程序员如何练就领导力》_刘朋_2018-08-01：[百度云链接](https://pan.baidu.com/s/1zXGly5l5c0jp5i4Uei5lQw) 提取码：t7g2
15.    《微管理-给你一个技术团队，你该怎么管》_杨立东：[百度云链接](https://pan.baidu.com/s/1oz2SriT-A7n5NST3uPbzrw) 提取码：jhmx
16.    《向技术管理者转型  软件开发人员跨越行业、技术、管理的转型思维与实践》：[百度云链接](https://pan.baidu.com/s/1I3SyEKE1Cq9pOxFmZXty2g) 提取码：9e94
17.    《小团队构建大网站：中小研发团队架构实践》_张辉清_2018-12-01：[百度云链接](https://pan.baidu.com/s/1DBzvmFOcD-5tH1Pv3eIj2Q) 提取码：c86t
18.    《项目管理》：[百度云链接](https://pan.baidu.com/s/1Hlb_n0X5XAfFjpLyuJRo2g) 提取码：3zr5


### 自然语言处理
0.    《大规模中文文本处理》：[百度云链接](https://pan.baidu.com/s/1dTP31U1lTS6NO_hotj4XWw) 提取码：7yvt
1.    《计算机自然语言处理》：[百度云链接](https://pan.baidu.com/s/1bT_7z10OdZ5AjnypCO-GRg) 提取码：kwk4
2.    《统计自然语言处理基础》：[百度云链接](https://pan.baidu.com/s/1EdXTBDONJUsPo7-zngd9YA) 提取码：6q3e
3.    《用Python进行自然语言处理》：[百度云链接](https://pan.baidu.com/s/1HDh08cIdSqHg6HgRZYcw7A) 提取码：gq9e
4.    《自然语言处理综论》：[百度云链接](https://pan.baidu.com/s/1Rc8LuWg0EUlaTg3DRsqg-Q) 提取码：i9hd
5.    《自然语言理解(第2版)》：[百度云链接](https://pan.baidu.com/s/1YKRvxj5AQKIndDahS5gbGQ) 提取码：3dia
6.    《python自然语言处理实战：核心技术与算法》：[百度云链接](https://pan.baidu.com/s/1E_r3yd6S1H0hDTJG8MrI-Q) 提取码：574y


## 免责声明
本软件和技术仅供学习和研究使用，禁止将其用于商业用途。如果您使用本软件和技术用于商业化而产生任何损失、风险或法律纠纷，作者将不承担任何责任。

作者不保证本软件和技术的完整性、准确性、可靠性、适用性或及时性。使用本软件和技术产生的任何后果，作者将不承担任何责任。

本软件和技术中的任何意见、建议或信息，均不构成作者或作者所在组织的任何形式的保证或担保。

任何人不得对本软件和技术进行反向工程、破解或其他非法行为。任何未经作者或作者所在组织授权的行为，均可能构成侵权或违法行为，一经发现作者将保留追究其法律责任的权利。

使用本软件和技术将被视为您已经完全接受本免责声明的所有条款和条件。如果您不同意本免责声明的任何部分，请勿使用本软件和技术。

## 公众号

最近整理了一份牛逼的学习资料，包括但不限于Java基础部分（JVM、Java集合框架、多线程），还囊括了 **数据库、计算机网络、算法与数据结构、设计模式、框架类Spring、Netty、微服务（Dubbo，消息队列） 网关** 等等等等……详情戳：[可以说是2023年全网最全的学习和找工作的PDF资源了](https://arcstack.top/#/home)

公众号 **顶尖架构师栈** 即可免费领取 无任何套路。

![](https://arcstack.top/tools/assets/images/wechat_subscribe.png)
